<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TabledataController extends Controller
{
    public function datatables(){
        return view('table.datatables');
    } 
}
