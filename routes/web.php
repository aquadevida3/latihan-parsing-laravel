<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*default ROUTE
Route::get('/', function () {//maksudnya route : method('url/urinya apa', function())
    return view('welcome'); //tampilkan view -> welcome (ada di folder view->welcome.blade.php)
});

Route::get('/go', function (){
    return view('ngetes.gogo');
});

*/

Route::get('/', 'IndexController@index' );

Route::get('/regis', 'AuthController@regis');

Route::post('/sent', 'AuthController@sent');

//Route::get('/adminlte', 'AdminlteController@adminlte');

Route::get('/tables', 'TableController@tables');

Route::get('/datatables', 'TabledataController@datatables');


