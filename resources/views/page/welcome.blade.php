{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>welcome</title>
</head>
<body> --}}
    @extends('layout.layout_adminlte')

    @section('judul')
    HALAMAN WELCOME    {{--INI BUAT JUDUL --}}
    @endsection
    
    @section('content')
    <h1>SELAMAT DATANG! {{$namaawal}} {{$namaakhir}}</h1>
    <h5>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h5>
    @endsection
{{-- </body>
</html> --}}